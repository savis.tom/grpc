package main

import (
	"context"
	"log"
	"time"

	pb "example.com/tung-pt/go-lang/user"
	"google.golang.org/grpc"
)

const (
	address = "localhost:30501"
)

func main() {

	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewUserClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.UserGetCertificates(ctx, &pb.RequestListCertificate{UserConnectId: "12312344"})

	if err != nil {
		log.Fatalf("could not create user: %v", err)
	}
	log.Printf(r.String())

}
