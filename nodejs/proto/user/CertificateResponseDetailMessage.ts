// Original file: proto/user.proto


export interface CertificateResponseDetailMessage {
  'id'?: (string);
  'createdDate'?: (string);
  'subjectDN'?: (string);
  'certificateBase64'?: (string);
  'validFrom'?: (string);
  'validTo'?: (string);
}

export interface CertificateResponseDetailMessage__Output {
  'id'?: (string);
  'createdDate'?: (string);
  'subjectDN'?: (string);
  'certificateBase64'?: (string);
  'validFrom'?: (string);
  'validTo'?: (string);
}
