// Original file: proto/user.proto

import type { CertificateResponseDetailMessage as _user_CertificateResponseDetailMessage, CertificateResponseDetailMessage__Output as _user_CertificateResponseDetailMessage__Output } from '../user/CertificateResponseDetailMessage';

export interface ListCertificateResponseDetailMessage {
  'data'?: (_user_CertificateResponseDetailMessage)[];
}

export interface ListCertificateResponseDetailMessage__Output {
  'data'?: (_user_CertificateResponseDetailMessage__Output)[];
}
