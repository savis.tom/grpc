// Original file: proto/user.proto


export interface CreateEFormFrom3rdResponseDataMessage {
  'eFormType'?: (string);
  'documentCode'?: (string);
  'fileUrl'?: (string);
  'identifierDevice'?: (string);
  'documentStatus'?: (number);
}

export interface CreateEFormFrom3rdResponseDataMessage__Output {
  'eFormType'?: (string);
  'documentCode'?: (string);
  'fileUrl'?: (string);
  'identifierDevice'?: (string);
  'documentStatus'?: (number);
}
