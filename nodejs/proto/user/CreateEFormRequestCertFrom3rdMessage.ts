// Original file: proto/user.proto

import type { UserModel as _common_UserModel, UserModel__Output as _common_UserModel__Output } from '../common/UserModel';
import type { LocationSignMessage as _common_LocationSignMessage, LocationSignMessage__Output as _common_LocationSignMessage__Output } from '../common/LocationSignMessage';
import type { OpratingSystemMobileMessage as _common_OpratingSystemMobileMessage, OpratingSystemMobileMessage__Output as _common_OpratingSystemMobileMessage__Output } from '../common/OpratingSystemMobileMessage';

export interface CreateEFormRequestCertFrom3rdMessage {
  'userConnectId'?: (string);
  'certValidTime'?: (string);
  'userModel'?: (_common_UserModel | null);
  'location'?: (_common_LocationSignMessage | null);
  'deviceInfo'?: (_common_OpratingSystemMobileMessage | null);
}

export interface CreateEFormRequestCertFrom3rdMessage__Output {
  'userConnectId'?: (string);
  'certValidTime'?: (string);
  'userModel'?: (_common_UserModel__Output);
  'location'?: (_common_LocationSignMessage__Output);
  'deviceInfo'?: (_common_OpratingSystemMobileMessage__Output);
}
