// Original file: proto/user.proto


export interface AddFirebaseRequestMessage3rd {
  'userConnectId'?: (string);
  'deviceId'?: (string);
  'firebaseToken'?: (string);
}

export interface AddFirebaseRequestMessage3rd__Output {
  'userConnectId'?: (string);
  'deviceId'?: (string);
  'firebaseToken'?: (string);
}
