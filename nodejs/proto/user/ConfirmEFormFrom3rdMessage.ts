// Original file: proto/user.proto

import type { LocationSignMessage as _common_LocationSignMessage, LocationSignMessage__Output as _common_LocationSignMessage__Output } from '../common/LocationSignMessage';
import type { OpratingSystemMobileMessage as _common_OpratingSystemMobileMessage, OpratingSystemMobileMessage__Output as _common_OpratingSystemMobileMessage__Output } from '../common/OpratingSystemMobileMessage';

export interface ConfirmEFormFrom3rdMessage {
  'userConnectId'?: (string);
  'documentCode'?: (string);
  'location'?: (_common_LocationSignMessage | null);
  'deviceInfo'?: (_common_OpratingSystemMobileMessage | null);
}

export interface ConfirmEFormFrom3rdMessage__Output {
  'userConnectId'?: (string);
  'documentCode'?: (string);
  'location'?: (_common_LocationSignMessage__Output);
  'deviceInfo'?: (_common_OpratingSystemMobileMessage__Output);
}
