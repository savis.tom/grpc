// Original file: proto/user.proto


export interface DeviceAddRequestFrom3rdMessage {
  'userConnectId'?: (string);
  'deviceId'?: (string);
  'deviceName'?: (string);
  'firebaseToken'?: (string);
  'isIdentifierDevice'?: (boolean);
}

export interface DeviceAddRequestFrom3rdMessage__Output {
  'userConnectId'?: (string);
  'deviceId'?: (string);
  'deviceName'?: (string);
  'firebaseToken'?: (string);
  'isIdentifierDevice'?: (boolean);
}
