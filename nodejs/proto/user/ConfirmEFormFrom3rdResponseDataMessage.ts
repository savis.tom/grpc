// Original file: proto/user.proto


export interface ConfirmEFormFrom3rdResponseDataMessage {
  'documentCode'?: (string);
  'fileUrl'?: (string);
}

export interface ConfirmEFormFrom3rdResponseDataMessage__Output {
  'documentCode'?: (string);
  'fileUrl'?: (string);
}
