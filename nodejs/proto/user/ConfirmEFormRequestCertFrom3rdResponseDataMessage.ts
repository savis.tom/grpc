// Original file: proto/user.proto


export interface ConfirmEFormRequestCertFrom3rdResponseDataMessage {
  'documentCode'?: (string);
  'fileUrl'?: (string);
  'certificateBase64'?: (string);
}

export interface ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output {
  'documentCode'?: (string);
  'fileUrl'?: (string);
  'certificateBase64'?: (string);
}
