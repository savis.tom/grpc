// Original file: proto/user.proto


export interface CreateEFormRequestCertFrom3rdResponseDataMessage {
  'eFormType'?: (string);
  'documentCode'?: (string);
  'fileUrl'?: (string);
}

export interface CreateEFormRequestCertFrom3rdResponseDataMessage__Output {
  'eFormType'?: (string);
  'documentCode'?: (string);
  'fileUrl'?: (string);
}
