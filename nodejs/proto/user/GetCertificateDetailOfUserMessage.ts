// Original file: proto/user.proto


export interface GetCertificateDetailOfUserMessage {
  'id'?: (string);
  'userConnectId'?: (string);
}

export interface GetCertificateDetailOfUserMessage__Output {
  'id'?: (string);
  'userConnectId'?: (string);
}
