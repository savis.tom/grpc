// Original file: proto/user.proto


export interface UserPinFromMessage {
  'userConnectId'?: (string);
  'PINCode'?: (string);
}

export interface UserPinFromMessage__Output {
  'userConnectId'?: (string);
  'PINCode'?: (string);
}
