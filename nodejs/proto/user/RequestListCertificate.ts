// Original file: proto/user.proto


export interface RequestListCertificate {
  'userConnectId'?: (string);
  'getAll'?: (boolean);
}

export interface RequestListCertificate__Output {
  'userConnectId'?: (string);
  'getAll'?: (boolean);
}
