// Original file: proto/user.proto


export interface GetCertificateDetailOfUserResponseDetailMessage {
  'id'?: (string);
  'createdDate'?: (string);
  'subjectDN'?: (string);
  'certificateBase64'?: (string);
  'validFrom'?: (string);
  'validTo'?: (string);
  'chainCertificateBase64'?: (string)[];
}

export interface GetCertificateDetailOfUserResponseDetailMessage__Output {
  'id'?: (string);
  'createdDate'?: (string);
  'subjectDN'?: (string);
  'certificateBase64'?: (string);
  'validFrom'?: (string);
  'validTo'?: (string);
  'chainCertificateBase64'?: (string)[];
}
