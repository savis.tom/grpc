// Original file: proto/user.proto

import type { UserModel as _common_UserModel, UserModel__Output as _common_UserModel__Output } from '../common/UserModel';

export interface UserRequest {
  'listUser'?: (_common_UserModel)[];
}

export interface UserRequest__Output {
  'listUser'?: (_common_UserModel__Output)[];
}
