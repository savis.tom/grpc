// Original file: proto/user.proto

import type * as grpc from '@grpc/grpc-js'
import type { MethodDefinition } from '@grpc/proto-loader'
import type { AddFirebaseRequestMessage3rd as _user_AddFirebaseRequestMessage3rd, AddFirebaseRequestMessage3rd__Output as _user_AddFirebaseRequestMessage3rd__Output } from '../user/AddFirebaseRequestMessage3rd';
import type { ConfirmEFormFrom3rdMessage as _user_ConfirmEFormFrom3rdMessage, ConfirmEFormFrom3rdMessage__Output as _user_ConfirmEFormFrom3rdMessage__Output } from '../user/ConfirmEFormFrom3rdMessage';
import type { ConfirmEFormFrom3rdResponseDataMessage as _user_ConfirmEFormFrom3rdResponseDataMessage, ConfirmEFormFrom3rdResponseDataMessage__Output as _user_ConfirmEFormFrom3rdResponseDataMessage__Output } from '../user/ConfirmEFormFrom3rdResponseDataMessage';
import type { ConfirmEFormRequestCertFrom3rdMessage as _user_ConfirmEFormRequestCertFrom3rdMessage, ConfirmEFormRequestCertFrom3rdMessage__Output as _user_ConfirmEFormRequestCertFrom3rdMessage__Output } from '../user/ConfirmEFormRequestCertFrom3rdMessage';
import type { ConfirmEFormRequestCertFrom3rdResponseDataMessage as _user_ConfirmEFormRequestCertFrom3rdResponseDataMessage, ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output as _user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output } from '../user/ConfirmEFormRequestCertFrom3rdResponseDataMessage';
import type { CreateEFormFrom3rdMessage as _user_CreateEFormFrom3rdMessage, CreateEFormFrom3rdMessage__Output as _user_CreateEFormFrom3rdMessage__Output } from '../user/CreateEFormFrom3rdMessage';
import type { CreateEFormFrom3rdResponseDataMessage as _user_CreateEFormFrom3rdResponseDataMessage, CreateEFormFrom3rdResponseDataMessage__Output as _user_CreateEFormFrom3rdResponseDataMessage__Output } from '../user/CreateEFormFrom3rdResponseDataMessage';
import type { CreateEFormRequestCertFrom3rdMessage as _user_CreateEFormRequestCertFrom3rdMessage, CreateEFormRequestCertFrom3rdMessage__Output as _user_CreateEFormRequestCertFrom3rdMessage__Output } from '../user/CreateEFormRequestCertFrom3rdMessage';
import type { CreateEFormRequestCertFrom3rdResponseDataMessage as _user_CreateEFormRequestCertFrom3rdResponseDataMessage, CreateEFormRequestCertFrom3rdResponseDataMessage__Output as _user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output } from '../user/CreateEFormRequestCertFrom3rdResponseDataMessage';
import type { DeviceAddRequestFrom3rdMessage as _user_DeviceAddRequestFrom3rdMessage, DeviceAddRequestFrom3rdMessage__Output as _user_DeviceAddRequestFrom3rdMessage__Output } from '../user/DeviceAddRequestFrom3rdMessage';
import type { GetCertificateDetailOfUserMessage as _user_GetCertificateDetailOfUserMessage, GetCertificateDetailOfUserMessage__Output as _user_GetCertificateDetailOfUserMessage__Output } from '../user/GetCertificateDetailOfUserMessage';
import type { GetCertificateDetailOfUserResponseDetailMessage as _user_GetCertificateDetailOfUserResponseDetailMessage, GetCertificateDetailOfUserResponseDetailMessage__Output as _user_GetCertificateDetailOfUserResponseDetailMessage__Output } from '../user/GetCertificateDetailOfUserResponseDetailMessage';
import type { ListCertificateResponseDetailMessage as _user_ListCertificateResponseDetailMessage, ListCertificateResponseDetailMessage__Output as _user_ListCertificateResponseDetailMessage__Output } from '../user/ListCertificateResponseDetailMessage';
import type { MessageResponse as _common_MessageResponse, MessageResponse__Output as _common_MessageResponse__Output } from '../common/MessageResponse';
import type { RequestListCertificate as _user_RequestListCertificate, RequestListCertificate__Output as _user_RequestListCertificate__Output } from '../user/RequestListCertificate';
import type { UserPinFromMessage as _user_UserPinFromMessage, UserPinFromMessage__Output as _user_UserPinFromMessage__Output } from '../user/UserPinFromMessage';
import type { UserRequest as _user_UserRequest, UserRequest__Output as _user_UserRequest__Output } from '../user/UserRequest';

export interface UserClient extends grpc.Client {
  UserAddDeviceFirebasetoken(argument: _user_AddFirebaseRequestMessage3rd, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserAddDeviceFirebasetoken(argument: _user_AddFirebaseRequestMessage3rd, metadata: grpc.Metadata, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserAddDeviceFirebasetoken(argument: _user_AddFirebaseRequestMessage3rd, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserAddDeviceFirebasetoken(argument: _user_AddFirebaseRequestMessage3rd, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userAddDeviceFirebasetoken(argument: _user_AddFirebaseRequestMessage3rd, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userAddDeviceFirebasetoken(argument: _user_AddFirebaseRequestMessage3rd, metadata: grpc.Metadata, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userAddDeviceFirebasetoken(argument: _user_AddFirebaseRequestMessage3rd, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userAddDeviceFirebasetoken(argument: _user_AddFirebaseRequestMessage3rd, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  
  UserConfirmEformRequestCertificate(argument: _user_ConfirmEFormRequestCertFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserConfirmEformRequestCertificate(argument: _user_ConfirmEFormRequestCertFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserConfirmEformRequestCertificate(argument: _user_ConfirmEFormRequestCertFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserConfirmEformRequestCertificate(argument: _user_ConfirmEFormRequestCertFrom3rdMessage, callback: grpc.requestCallback<_user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userConfirmEformRequestCertificate(argument: _user_ConfirmEFormRequestCertFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userConfirmEformRequestCertificate(argument: _user_ConfirmEFormRequestCertFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userConfirmEformRequestCertificate(argument: _user_ConfirmEFormRequestCertFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userConfirmEformRequestCertificate(argument: _user_ConfirmEFormRequestCertFrom3rdMessage, callback: grpc.requestCallback<_user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  
  UserConfirmEformUseDigital(argument: _user_ConfirmEFormFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ConfirmEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserConfirmEformUseDigital(argument: _user_ConfirmEFormFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_ConfirmEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserConfirmEformUseDigital(argument: _user_ConfirmEFormFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ConfirmEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserConfirmEformUseDigital(argument: _user_ConfirmEFormFrom3rdMessage, callback: grpc.requestCallback<_user_ConfirmEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userConfirmEformUseDigital(argument: _user_ConfirmEFormFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ConfirmEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userConfirmEformUseDigital(argument: _user_ConfirmEFormFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_ConfirmEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userConfirmEformUseDigital(argument: _user_ConfirmEFormFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ConfirmEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userConfirmEformUseDigital(argument: _user_ConfirmEFormFrom3rdMessage, callback: grpc.requestCallback<_user_ConfirmEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  
  UserCreateEformRequestCertificate(argument: _user_CreateEFormRequestCertFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserCreateEformRequestCertificate(argument: _user_CreateEFormRequestCertFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserCreateEformRequestCertificate(argument: _user_CreateEFormRequestCertFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserCreateEformRequestCertificate(argument: _user_CreateEFormRequestCertFrom3rdMessage, callback: grpc.requestCallback<_user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userCreateEformRequestCertificate(argument: _user_CreateEFormRequestCertFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userCreateEformRequestCertificate(argument: _user_CreateEFormRequestCertFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userCreateEformRequestCertificate(argument: _user_CreateEFormRequestCertFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userCreateEformRequestCertificate(argument: _user_CreateEFormRequestCertFrom3rdMessage, callback: grpc.requestCallback<_user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  
  UserCreateEformUseDigital(argument: _user_CreateEFormFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_CreateEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserCreateEformUseDigital(argument: _user_CreateEFormFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_CreateEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserCreateEformUseDigital(argument: _user_CreateEFormFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_CreateEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  UserCreateEformUseDigital(argument: _user_CreateEFormFrom3rdMessage, callback: grpc.requestCallback<_user_CreateEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userCreateEformUseDigital(argument: _user_CreateEFormFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_CreateEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userCreateEformUseDigital(argument: _user_CreateEFormFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_CreateEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userCreateEformUseDigital(argument: _user_CreateEFormFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_CreateEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  userCreateEformUseDigital(argument: _user_CreateEFormFrom3rdMessage, callback: grpc.requestCallback<_user_CreateEFormFrom3rdResponseDataMessage__Output>): grpc.ClientUnaryCall;
  
  UserGetCertificateDetail(argument: _user_GetCertificateDetailOfUserMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_GetCertificateDetailOfUserResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  UserGetCertificateDetail(argument: _user_GetCertificateDetailOfUserMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_GetCertificateDetailOfUserResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  UserGetCertificateDetail(argument: _user_GetCertificateDetailOfUserMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_GetCertificateDetailOfUserResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  UserGetCertificateDetail(argument: _user_GetCertificateDetailOfUserMessage, callback: grpc.requestCallback<_user_GetCertificateDetailOfUserResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  userGetCertificateDetail(argument: _user_GetCertificateDetailOfUserMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_GetCertificateDetailOfUserResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  userGetCertificateDetail(argument: _user_GetCertificateDetailOfUserMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_GetCertificateDetailOfUserResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  userGetCertificateDetail(argument: _user_GetCertificateDetailOfUserMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_user_GetCertificateDetailOfUserResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  userGetCertificateDetail(argument: _user_GetCertificateDetailOfUserMessage, callback: grpc.requestCallback<_user_GetCertificateDetailOfUserResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  
  UserGetCertificates(argument: _user_RequestListCertificate, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ListCertificateResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  UserGetCertificates(argument: _user_RequestListCertificate, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_ListCertificateResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  UserGetCertificates(argument: _user_RequestListCertificate, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ListCertificateResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  UserGetCertificates(argument: _user_RequestListCertificate, callback: grpc.requestCallback<_user_ListCertificateResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  userGetCertificates(argument: _user_RequestListCertificate, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ListCertificateResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  userGetCertificates(argument: _user_RequestListCertificate, metadata: grpc.Metadata, callback: grpc.requestCallback<_user_ListCertificateResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  userGetCertificates(argument: _user_RequestListCertificate, options: grpc.CallOptions, callback: grpc.requestCallback<_user_ListCertificateResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  userGetCertificates(argument: _user_RequestListCertificate, callback: grpc.requestCallback<_user_ListCertificateResponseDetailMessage__Output>): grpc.ClientUnaryCall;
  
  UserSynchronizedInformation(argument: _user_UserRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserSynchronizedInformation(argument: _user_UserRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserSynchronizedInformation(argument: _user_UserRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserSynchronizedInformation(argument: _user_UserRequest, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userSynchronizedInformation(argument: _user_UserRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userSynchronizedInformation(argument: _user_UserRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userSynchronizedInformation(argument: _user_UserRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userSynchronizedInformation(argument: _user_UserRequest, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  
  UserUpdateDeviceIdentifier(argument: _user_DeviceAddRequestFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserUpdateDeviceIdentifier(argument: _user_DeviceAddRequestFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserUpdateDeviceIdentifier(argument: _user_DeviceAddRequestFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserUpdateDeviceIdentifier(argument: _user_DeviceAddRequestFrom3rdMessage, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userUpdateDeviceIdentifier(argument: _user_DeviceAddRequestFrom3rdMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userUpdateDeviceIdentifier(argument: _user_DeviceAddRequestFrom3rdMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userUpdateDeviceIdentifier(argument: _user_DeviceAddRequestFrom3rdMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userUpdateDeviceIdentifier(argument: _user_DeviceAddRequestFrom3rdMessage, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  
  UserUpdatePinCode(argument: _user_UserPinFromMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserUpdatePinCode(argument: _user_UserPinFromMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserUpdatePinCode(argument: _user_UserPinFromMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  UserUpdatePinCode(argument: _user_UserPinFromMessage, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userUpdatePinCode(argument: _user_UserPinFromMessage, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userUpdatePinCode(argument: _user_UserPinFromMessage, metadata: grpc.Metadata, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userUpdatePinCode(argument: _user_UserPinFromMessage, options: grpc.CallOptions, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  userUpdatePinCode(argument: _user_UserPinFromMessage, callback: grpc.requestCallback<_common_MessageResponse__Output>): grpc.ClientUnaryCall;
  
}

export interface UserHandlers extends grpc.UntypedServiceImplementation {
  UserAddDeviceFirebasetoken: grpc.handleUnaryCall<_user_AddFirebaseRequestMessage3rd__Output, _common_MessageResponse>;
  
  UserConfirmEformRequestCertificate: grpc.handleUnaryCall<_user_ConfirmEFormRequestCertFrom3rdMessage__Output, _user_ConfirmEFormRequestCertFrom3rdResponseDataMessage>;
  
  UserConfirmEformUseDigital: grpc.handleUnaryCall<_user_ConfirmEFormFrom3rdMessage__Output, _user_ConfirmEFormFrom3rdResponseDataMessage>;
  
  UserCreateEformRequestCertificate: grpc.handleUnaryCall<_user_CreateEFormRequestCertFrom3rdMessage__Output, _user_CreateEFormRequestCertFrom3rdResponseDataMessage>;
  
  UserCreateEformUseDigital: grpc.handleUnaryCall<_user_CreateEFormFrom3rdMessage__Output, _user_CreateEFormFrom3rdResponseDataMessage>;
  
  UserGetCertificateDetail: grpc.handleUnaryCall<_user_GetCertificateDetailOfUserMessage__Output, _user_GetCertificateDetailOfUserResponseDetailMessage>;
  
  UserGetCertificates: grpc.handleUnaryCall<_user_RequestListCertificate__Output, _user_ListCertificateResponseDetailMessage>;
  
  UserSynchronizedInformation: grpc.handleUnaryCall<_user_UserRequest__Output, _common_MessageResponse>;
  
  UserUpdateDeviceIdentifier: grpc.handleUnaryCall<_user_DeviceAddRequestFrom3rdMessage__Output, _common_MessageResponse>;
  
  UserUpdatePinCode: grpc.handleUnaryCall<_user_UserPinFromMessage__Output, _common_MessageResponse>;
  
}

export interface UserDefinition extends grpc.ServiceDefinition {
  UserAddDeviceFirebasetoken: MethodDefinition<_user_AddFirebaseRequestMessage3rd, _common_MessageResponse, _user_AddFirebaseRequestMessage3rd__Output, _common_MessageResponse__Output>
  UserConfirmEformRequestCertificate: MethodDefinition<_user_ConfirmEFormRequestCertFrom3rdMessage, _user_ConfirmEFormRequestCertFrom3rdResponseDataMessage, _user_ConfirmEFormRequestCertFrom3rdMessage__Output, _user_ConfirmEFormRequestCertFrom3rdResponseDataMessage__Output>
  UserConfirmEformUseDigital: MethodDefinition<_user_ConfirmEFormFrom3rdMessage, _user_ConfirmEFormFrom3rdResponseDataMessage, _user_ConfirmEFormFrom3rdMessage__Output, _user_ConfirmEFormFrom3rdResponseDataMessage__Output>
  UserCreateEformRequestCertificate: MethodDefinition<_user_CreateEFormRequestCertFrom3rdMessage, _user_CreateEFormRequestCertFrom3rdResponseDataMessage, _user_CreateEFormRequestCertFrom3rdMessage__Output, _user_CreateEFormRequestCertFrom3rdResponseDataMessage__Output>
  UserCreateEformUseDigital: MethodDefinition<_user_CreateEFormFrom3rdMessage, _user_CreateEFormFrom3rdResponseDataMessage, _user_CreateEFormFrom3rdMessage__Output, _user_CreateEFormFrom3rdResponseDataMessage__Output>
  UserGetCertificateDetail: MethodDefinition<_user_GetCertificateDetailOfUserMessage, _user_GetCertificateDetailOfUserResponseDetailMessage, _user_GetCertificateDetailOfUserMessage__Output, _user_GetCertificateDetailOfUserResponseDetailMessage__Output>
  UserGetCertificates: MethodDefinition<_user_RequestListCertificate, _user_ListCertificateResponseDetailMessage, _user_RequestListCertificate__Output, _user_ListCertificateResponseDetailMessage__Output>
  UserSynchronizedInformation: MethodDefinition<_user_UserRequest, _common_MessageResponse, _user_UserRequest__Output, _common_MessageResponse__Output>
  UserUpdateDeviceIdentifier: MethodDefinition<_user_DeviceAddRequestFrom3rdMessage, _common_MessageResponse, _user_DeviceAddRequestFrom3rdMessage__Output, _common_MessageResponse__Output>
  UserUpdatePinCode: MethodDefinition<_user_UserPinFromMessage, _common_MessageResponse, _user_UserPinFromMessage__Output, _common_MessageResponse__Output>
}
