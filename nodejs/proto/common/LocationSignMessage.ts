// Original file: proto/common.proto


export interface LocationSignMessage {
  'latitude'?: (number | string);
  'longitude'?: (number | string);
  'geoLocation'?: (string);
}

export interface LocationSignMessage__Output {
  'latitude'?: (number);
  'longitude'?: (number);
  'geoLocation'?: (string);
}
