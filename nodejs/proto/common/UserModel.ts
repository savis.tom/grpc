// Original file: proto/common.proto


export interface UserModel {
  'userConnectId'?: (string);
  'userFullName'?: (string);
  'birthday'?: (string);
  'gender'?: (number);
  'identityType'?: (string);
  'identityNumber'?: (string);
  'issueDate'?: (string);
  'issueBy'?: (string);
  'userPhoneNumber'?: (string);
  'userEmail'?: (string);
  'address'?: (string);
  'permanentAddress'?: (string);
  'countryName'?: (string);
  'provinceName'?: (string);
  'districtName'?: (string);
}

export interface UserModel__Output {
  'userConnectId'?: (string);
  'userFullName'?: (string);
  'birthday'?: (string);
  'gender'?: (number);
  'identityType'?: (string);
  'identityNumber'?: (string);
  'issueDate'?: (string);
  'issueBy'?: (string);
  'userPhoneNumber'?: (string);
  'userEmail'?: (string);
  'address'?: (string);
  'permanentAddress'?: (string);
  'countryName'?: (string);
  'provinceName'?: (string);
  'districtName'?: (string);
}
