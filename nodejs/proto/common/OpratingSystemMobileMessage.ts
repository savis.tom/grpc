// Original file: proto/common.proto


export interface OpratingSystemMobileMessage {
  'appCodeName'?: (string);
  'appName'?: (string);
  'appVersion'?: (string);
  'appType'?: (string);
  'language'?: (string);
  'deviceType'?: (string);
  'deviceId'?: (string);
  'ipAddress'?: (string);
  'macAddress'?: (string);
  'deviceName'?: (string);
}

export interface OpratingSystemMobileMessage__Output {
  'appCodeName'?: (string);
  'appName'?: (string);
  'appVersion'?: (string);
  'appType'?: (string);
  'language'?: (string);
  'deviceType'?: (string);
  'deviceId'?: (string);
  'ipAddress'?: (string);
  'macAddress'?: (string);
  'deviceName'?: (string);
}
