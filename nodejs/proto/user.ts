import type * as grpc from '@grpc/grpc-js';
import type { MessageTypeDefinition } from '@grpc/proto-loader';

import type { UserClient as _user_UserClient, UserDefinition as _user_UserDefinition } from './user/User';

type SubtypeConstructor<Constructor extends new (...args: any) => any, Subtype> = {
  new(...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  common: {
    LocationSignMessage: MessageTypeDefinition
    MessageResponse: MessageTypeDefinition
    OpratingSystemMobileMessage: MessageTypeDefinition
    UserModel: MessageTypeDefinition
  }
  user: {
    AddFirebaseRequestMessage3rd: MessageTypeDefinition
    CertificateResponseDetailMessage: MessageTypeDefinition
    ConfirmEFormFrom3rdMessage: MessageTypeDefinition
    ConfirmEFormFrom3rdResponseDataMessage: MessageTypeDefinition
    ConfirmEFormRequestCertFrom3rdMessage: MessageTypeDefinition
    ConfirmEFormRequestCertFrom3rdResponseDataMessage: MessageTypeDefinition
    CreateEFormFrom3rdMessage: MessageTypeDefinition
    CreateEFormFrom3rdResponseDataMessage: MessageTypeDefinition
    CreateEFormRequestCertFrom3rdMessage: MessageTypeDefinition
    CreateEFormRequestCertFrom3rdResponseDataMessage: MessageTypeDefinition
    DeviceAddRequestFrom3rdMessage: MessageTypeDefinition
    GetCertificateDetailOfUserMessage: MessageTypeDefinition
    GetCertificateDetailOfUserResponseDetailMessage: MessageTypeDefinition
    ListCertificateResponseDetailMessage: MessageTypeDefinition
    RequestListCertificate: MessageTypeDefinition
    User: SubtypeConstructor<typeof grpc.Client, _user_UserClient> & { service: _user_UserDefinition }
    UserPinFromMessage: MessageTypeDefinition
    UserRequest: MessageTypeDefinition
  }
}

